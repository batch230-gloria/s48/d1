// mock database
let posts = []; // empty collection

let countAsId = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	// Thist line of code prevents our page from refreshing
	event.preventDefault();

	posts.push(
		{
			id: countAsId,
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value
		}
	)

	// For the next document have different id
	countAsId++;

	console.log("Updated posts array after adding post: ");
	console.log(posts);

	showPosts(posts);
	alert("Successfully added");

})

// Show all posts
const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		// div >> post-1
		// h3 >> post-title-1
		postEntries +=`
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Post
// Edit post is activated from a button created in show posts
const editPost = (id) =>{

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}

// Update post

document.querySelector("#form-edit-post").addEventListener('submit', (event)=>{
	event.preventDefault();

	for(let i=0; i<posts.length; i++){
		if(posts[i].id.toString()=== document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
		}

		showPosts(posts);
		alert('Successfully updated');
		break
	}
})


// Activity
// Create a function called deletePost (const deletePost = (id) => {}) the function should be able to delete a specific post from posts array
/* 
    1. Remove in the posts array
    - An item with the same id number from the posts array will be removed upon clicking the delete button
        - You can use array methods as filter() or findIndex() and splice()
        - Show the results of updated array with the removed post through console.log(posts)
	2. Remove in the actual web page = showPosts(posts)
    - Then also, remove the element from the DOM (from the browser display) by first selecting the element and using the remove() method.
    
    https://www.w3schools.com/jsref/met_element_remove.asp
*/

// Delete Post
const deletePost = (id) =>{
	let deleteId = posts.findIndex((post) => post.id == id);
	posts.splice(deleteId, 1);
	showPosts(posts);
	console.log(posts);
}


